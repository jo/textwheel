# Changelog

## Unreleased

### Added

- Installable en tant que package Composer

### Changed

- Utilisation du collecteur `HtmlTag` (refactor).
- Compatible SPIP 5.0.0-dev

